//
//  TIReportViewModelTests.swift
//  RxSwiftTableDemoTests
//
//  Created by joseewu on 2018/10/31.
//  Copyright © 2018 com.isunClub. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RxTest
import RxBlocking
//add new testable module
@testable import RxSwiftTableDemo
class TIReportViewModelTests: XCTestCase {
    var disposeBag:DisposeBag = DisposeBag()
    var scheduler: TestScheduler!
    var subscription: Disposable!
    //declare view model as a variables and the assign value when hit specific testing block
    var reportViewModel = TIReportViewModel()
    override func setUp() {
         scheduler = TestScheduler(initialClock: 0)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        reportViewModel = TIReportViewModel()
        disposeBag = DisposeBag()
        scheduler.scheduleAt(1000) {
            self.subscription.dispose()
        }
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testCreateReport() {

        let userNameSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let problemSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let buttonTapSubject:PublishSubject<Void> = PublishSubject()

        userNameSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.userName).disposed(by: disposeBag)
         problemSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.problem).disposed(by: disposeBag)
        buttonTapSubject.bind(to: reportViewModel.didTapButton).disposed(by: disposeBag)
        userNameSubject.onNext("josee")
        problemSubject.onNext("josee")
        reportViewModel.output.newReport.asObservable().subscribe(onNext: { report in
            XCTAssert(report?.userName != nil)
            XCTAssert(report?.content != nil)
        }).disposed(by: disposeBag)
        buttonTapSubject.onNext(())
        buttonTapSubject.disposed(by: disposeBag)
    }
    func testCreateReportByRxTest() {
        let buttonTaps = scheduler.createHotObservable([next(100, ())])
        buttonTaps.bind(to: reportViewModel.didTapButton).disposed(by: disposeBag)
        let observer = scheduler.createObserver(Report?.self)
        let result = observer.events.map {
            return $0.value.element ?? nil
        }
        let expectedResult:[Report?] = []
        reportViewModel.newReport.drive(observer).disposed(by: disposeBag)
        scheduler.start()
        XCTAssertEqual(result, expectedResult)
    }
    func testCreateReportWithNilInput() {

        let userNameSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let problemSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let buttonTapSubject:PublishSubject<Void> = PublishSubject()
        buttonTapSubject.disposed(by: disposeBag)

        userNameSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.userName).disposed(by: disposeBag)
        problemSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.problem).disposed(by: disposeBag)
        buttonTapSubject.bind(to: reportViewModel.didTapButton).disposed(by: disposeBag)

        userNameSubject.onNext("josee")
        problemSubject.onNext(nil)
        reportViewModel.output.newReport.asObservable().subscribe(onNext: { (report) in
            XCTAssert(report == nil)
        }).disposed(by: disposeBag)
        //sending my content
        buttonTapSubject.onNext(())

    }
    func testEnterNillProblem() {

        let userNameSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let problemSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let buttonTapSubject:PublishSubject<Void> = PublishSubject()

        buttonTapSubject.disposed(by: disposeBag)
        userNameSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.userName).disposed(by: disposeBag)
        problemSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.problem).disposed(by: disposeBag)
        buttonTapSubject.bind(to: reportViewModel.didTapButton).disposed(by: disposeBag)
        userNameSubject.onNext("jfidfjosf")
        problemSubject.onNext(nil)
        reportViewModel.output.newReport.asObservable().subscribe(onNext: { report in
            XCTAssert(report == nil)
        }).disposed(by: disposeBag)
        buttonTapSubject.onNext(())
    }
    func testDeleteProblemContent() {
        let oldReport = Report(title: "no prob", content: "no prob", userName: "josee", printScreen: nil)
        reportViewModel = TIReportViewModel(with: oldReport)

        let userNameSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let problemSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let buttonTapSubject:PublishSubject<Void> = PublishSubject()
        buttonTapSubject.disposed(by: disposeBag)

        userNameSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.userName).disposed(by: disposeBag)
        problemSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.problem).disposed(by: disposeBag)
        buttonTapSubject.bind(to: reportViewModel.didTapButton).disposed(by: disposeBag)

        userNameSubject.onNext("josee")
        problemSubject.onNext("fdsfdsjfoisd")
        reportViewModel.output.newReport.asObservable().subscribe(onNext: { report in
            XCTAssert(report == nil)
        }).disposed(by: disposeBag)
        problemSubject.onNext(nil)
        buttonTapSubject.onNext(())
        //sending my content
    }
    func testCreateReportWithoutUserName() {
        let userNameSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let problemSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let buttonTapSubject:PublishSubject<Void> = PublishSubject()
        buttonTapSubject.disposed(by: disposeBag)

        userNameSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.userName).disposed(by: disposeBag)
        problemSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.problem).disposed(by: disposeBag)
        buttonTapSubject.bind(to: reportViewModel.didTapButton).disposed(by: disposeBag)

        userNameSubject.onNext(nil)
        problemSubject.onNext("fdsfdsjfoisd")
        reportViewModel.output.newReport.asObservable().subscribe(onNext: { report in
            XCTAssert(report?.userName == "no name")
            XCTAssert(report?.content != nil)
        }).disposed(by: disposeBag)
        buttonTapSubject.onNext(())
    }
    func testCreateReportUserNameIsEmpty() {

        let userNameSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let problemSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let buttonTapSubject:PublishSubject<Void> = PublishSubject()
        buttonTapSubject.disposed(by: disposeBag)

        userNameSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.userName).disposed(by: disposeBag)
        problemSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.problem).disposed(by: disposeBag)
        buttonTapSubject.bind(to: reportViewModel.didTapButton).disposed(by: disposeBag)

        userNameSubject.onNext(nil)
        problemSubject.onNext("fdsfdsjfoisd")
        reportViewModel.output.newReport.asObservable().subscribe(onNext: { report in
            XCTAssert(report?.userName == "no name")
            XCTAssert(report?.content != nil)
        }).disposed(by: disposeBag)
        buttonTapSubject.onNext(())
    }
    func testModifiedReport() {
        let userNameSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let problemSubject:BehaviorSubject<String?> = BehaviorSubject(value: "")
        let buttonTapSubject:PublishSubject<Void> = PublishSubject()
        buttonTapSubject.disposed(by: disposeBag)

        userNameSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.userName).disposed(by: disposeBag)
        problemSubject.asDriver(onErrorJustReturn: nil).drive(reportViewModel.problem).disposed(by: disposeBag)
        buttonTapSubject.bind(to: reportViewModel.didTapButton).disposed(by: disposeBag)

        userNameSubject.onNext(nil)
        problemSubject.onNext("fdsfdsjfoisd")
        reportViewModel.output.newReport.asObservable().subscribe(onNext: { report in
            XCTAssert(report?.content != "no prob")
        }).disposed(by: disposeBag)
        buttonTapSubject.onNext(())
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
        }
    }

}
