//
//  TIHomeViewModel.swift
//  RxSwiftTableDemo
//
//  Created by Josee Wu on 2018/10/24.
//  Copyright © 2018 com.isunClub. All rights reserved.
//
import Foundation
import RxSwift
import RxCocoa

protocol TIHomeViewModelDependencyType: DependencyType {
    associatedtype Dependency
    init(with client:Dependency)
}
protocol TIHomeViewModelOutputType: OutputType {
    var listData:Variable<[Report]>! {get}
}
class TIHomeViewModel:TIHomeViewModelDependencyType,TIHomeViewModelOutputType {
    var output:TIHomeViewModelOutputType{return self}
    var listData: Variable<[Report]>!
    required init(with client: TIProblemDatabase) {
        client.getAllReports()
        listData = Variable.init([Report]())
    }

    let disposeBag:DisposeBag = DisposeBag()
}
