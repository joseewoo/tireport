//
//  TIViewController.swift
//  RxSwiftTableDemo
//
//  Created by Josee Wu on 2018/10/23.
//  Copyright © 2018 com.isunClub. All rights reserved.
//
// https://medium.com/blablacar-tech/rxswift-mvvm-66827b8b3f10

import UIKit
import RxSwift
import RxCocoa

class TIViewController: UIViewController {
    @IBOutlet weak var floatyButton: TLFloatButton! {
        didSet {
            floatyButton.bgColor = UIColor.black
            floatyButton.iconImg = UIImage(named: "plus_icon")?.withRenderingMode(.alwaysTemplate)
            floatyButton.tintColor = UIColor.white
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.separatorStyle = .singleLine
            tableView.separatorColor = UIColor(red: 255/255, green: 130/255, blue: 126/255, alpha: 1)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.register(UINib(nibName: "TIItemTableViewCell", bundle: nil), forCellReuseIdentifier: "TIItemTableViewCell")
        }
    }
    @IBAction func addNewReport(_ sender: Any) {
        let reportVC = TIReporViewController(nibName:"TIReporViewController", bundle:nil)
        let injection:TIReporViewControllerInjection = TIReporViewControllerInjection(oldReport: nil)
        reportVC.inject(with: injection)
        reportVC.delegate = self
        navigationController?.pushViewController(reportVC, animated: true)
    }
    func showEditReportPage(_ report:Report) {
        let reportVC = TIReporViewController(nibName:"TIReporViewController", bundle:nil)
        let injection:TIReporViewControllerInjection = TIReporViewControllerInjection(oldReport: report)
        reportVC.delegate = self
        reportVC.inject(with: injection)
        navigationController?.pushViewController(reportVC, animated: true)
    }
    public var dependency:TIProblemDatabase = TIProblemDatabase()
    private var viewModel: ViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Tideisun report system"
        viewModel = TIHomeViewModel(with: dependency)
        viewModel.output.listData.asObservable().subscribe({ [weak self] report in
            self?.tableView.reloadData()
        }).disposed(by: disposeBag)
    }

//***ViewModel part
    typealias ViewModel = TIHomeViewModel
    let disposeBag: DisposeBag = DisposeBag()

}
extension TIViewController:TIReporViewControllerDelegate {
    func addNewReport(_ report: Report) {
        var old = viewModel.listData.value
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            old[selectedIndexPath.row] = report
        } else {
            old.append(report)
        }
        viewModel.output.listData.value = old

    }
}
extension TIViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.output.listData.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TIItemTableViewCell", for: indexPath) as? TIItemTableViewCell
        let data = viewModel.output.listData.value[indexPath.row]
        cell?.content.text = data.content
        cell?.title.text = data.userName
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedReport = viewModel.output.listData.value[indexPath.row]
        showEditReportPage(selectedReport)

    }
}
