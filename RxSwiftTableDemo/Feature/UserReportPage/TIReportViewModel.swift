//
//  TIReportViewModel.swift
//  RxSwiftTableDemo
//
//  Created by Josee Wu on 2018/10/25.
//  Copyright © 2018 com.isunClub. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol TIReportInputViewModel:InputType {
    var didTapButton: PublishSubject<Void> {get}
    var userName: BehaviorSubject<String?> {get}
    var problem: BehaviorSubject<String?> {get}
}

protocol TIReportOutputViewModel:OutputType {
    var newReport: Driver<Report?> {get}
    var oldReport: Report? {get set}
}

protocol TIReportDependencyType:DependencyType {
    init(with dependency:Report)
}

class TIReportViewModel: TIReportInputViewModel,TIReportOutputViewModel,TIReportDependencyType {

    var didTapButton: PublishSubject<Void> = PublishSubject.init()

    var userName: BehaviorSubject<String?> = BehaviorSubject.init(value: nil)

    var problem: BehaviorSubject<String?> = BehaviorSubject.init(value: nil)

    internal var oldReport: Report?

    var output:TIReportOutputViewModel {return self}

    var newReport: Driver<Report?> {
        return newReportSubject.asObserver().asDriver(onErrorJustReturn: nil)
    }
    let disposeBag:DisposeBag = DisposeBag()

    //report output
    private let newReportSubject:PublishSubject<Report?> = PublishSubject()

    init() {
        didTapButton
            .asObservable()
            .withLatestFrom(userName.asObservable())
            .withLatestFrom(problem.asObservable()) { (userName, problem) -> Report? in
                guard let problem = problem, problem.count != 0 else {
                    return nil }
                let new = Report(title: problem, content: problem, userName: userName ?? "no name", printScreen: nil)
                return new
            }.asDriver(onErrorJustReturn: nil)
            .drive(newReportSubject.asObserver())
            .disposed(by: disposeBag)
    }
    //when you need dependency injection
    required init(with dependency: Report) {
        self.oldReport = dependency
        didTapButton
            .asObservable()
            .withLatestFrom(userName.asObservable()).debug()
            .withLatestFrom(problem.asObservable()) { (userName, problem) -> Report? in
                guard let problem = problem, problem.count != 0 else {
                    return nil }
                let new = Report(title: problem, content: problem, userName: userName ?? "no name", printScreen: nil)
                return new
            }.asDriver(onErrorJustReturn: nil)
            .drive(newReportSubject.asObserver())
            .disposed(by: disposeBag)
    }
}
