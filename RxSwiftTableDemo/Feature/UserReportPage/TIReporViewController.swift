//
//  TIReporViewController.swift
//  RxSwiftTableDemo
//
//  Created by Josee Wu on 2018/10/25.
//  Copyright © 2018 com.isunClub. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
protocol TIReporViewControllerDelegate:class {
    func addNewReport(_ report:Report)
}
struct TIReporViewControllerInjection {
    let oldReport:Report?
}
class TIReporViewController: UIViewController {
    var disposeBag: DisposeBag = DisposeBag()
    var viewModel: TIReportViewModel = TIReportViewModel()
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var problem: UITextField!
    
    @IBOutlet weak var sendButton: UIButton!

    weak var delegate:TIReporViewControllerDelegate?

    //func inject
    public func inject(with injection:TIReporViewControllerInjection) {
        if let oldReport = injection.oldReport {
            viewModel = TIReportViewModel(with: oldReport)
        } else {
            viewModel = TIReportViewModel()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

    func setUp() {
        //先給值
        userName.text = viewModel.oldReport?.userName
        problem.text = viewModel.oldReport?.content
        //drive view model
        userName.rx.text.asDriver().drive(viewModel.userName)
            .disposed(by: disposeBag)

        problem.rx.text.asDriver().drive(viewModel.problem)
            .disposed(by: disposeBag)

        sendButton.rx.tap.asObservable().bind(to: viewModel.didTapButton)
            .disposed(by: disposeBag)
        //output
        viewModel.output.newReport.asObservable().subscribe(onNext: { [weak self] report in
            guard let report = report else {return}
            self?.navigationController?.popViewController(animated: true)
            self?.delegate?.addNewReport(report)
        }).disposed(by: disposeBag)
    }
}
