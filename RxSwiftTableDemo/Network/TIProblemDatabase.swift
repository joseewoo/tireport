//
//  TIProblemDatabase.swift
//  RxSwiftTableDemo
//
//  Created by joseewu on 2018/10/31.
//  Copyright © 2018 com.isunClub. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseCore

class TIProblemDatabase {
    let ref = Database.database().reference()
    func getAllReports() {
        ref.child("data").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [[String:Any]] {
                print(data)
//                let decoder = JSONDecoder.init()
//
//                do {
//                    let result = decoder.decode(<#T##type: Decodable.Protocol##Decodable.Protocol#>, from: <#T##Data#>)
//                } catch {
//
//                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
